from multiprocessing import connection
from flask import Flask, jsonify, render_template, request
import psycopg2


from wtforms import StringField, Form

connection = psycopg2.connect(
    host="localhost",
    user="postgres",
    password="postgres",
    database="postgres",
    port="5435",
)

connection.autocommit = True

app = Flask(__name__)


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/getdata", methods=["GET"])
def get_data():
    return jsonify({"main.html": ["cosita", "cosita2", "cosita3"]})


@app.route("/form", methods=["GET"])
def form_page():
    return render_template("form.html")


@app.route("/getform", methods=["POST"])
def get_form():
    name = request.form["name"]
    email = request.form["email"]
    phone = int(request.form["phone"])
    print(name, email, phone)
    cursor = connection.cursor()
    query = f"""
    INSERT INTO "user" ("name", email, phone) VALUES('{name}', '{email}', '{phone}');
    """
    print("\n", query)
    cursor.execute(query)
    cursor.close()
    return jsonify({"response": "OK"})


@app.route("/countries", methods=["GET"])
def countries_response():
    return jsonify({"countries": ["colombia", "venezuela", "ecuador"]})


class SearchForm(Form):
    autocomplete = StringField("write country", id="country_autocomplete")


@app.route("/autocomplete")
def autocomplete():
    form = SearchForm(request.form)
    return render_template("autocomplete.html", form=form)


if __name__ == "__main__":
    app.run(debug=True, port=5000)
